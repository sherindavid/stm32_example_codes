#include "ldr.h"
#include "adc_if.h"

uint16_t readLdr ()
{
	uint16_t lightIntensity = ADC_ReadChannels(ADC_CHANNEL_4);
	return (lightIntensity);
}

void readldrparameters(sensor_data *ldr_data){
	ldr_data-> lightIntensity = readLdr();
}
