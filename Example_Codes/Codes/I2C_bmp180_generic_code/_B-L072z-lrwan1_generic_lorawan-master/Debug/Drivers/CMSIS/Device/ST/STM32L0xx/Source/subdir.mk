################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Drivers/CMSIS/Device/ST/STM32L0xx/Source/system_stm32l0xx.c 

OBJS += \
./Drivers/CMSIS/Device/ST/STM32L0xx/Source/system_stm32l0xx.o 

C_DEPS += \
./Drivers/CMSIS/Device/ST/STM32L0xx/Source/system_stm32l0xx.d 


# Each subdirectory must supply rules for building sources it contributes
Drivers/CMSIS/Device/ST/STM32L0xx/Source/%.o Drivers/CMSIS/Device/ST/STM32L0xx/Source/%.su Drivers/CMSIS/Device/ST/STM32L0xx/Source/%.cyclo: ../Drivers/CMSIS/Device/ST/STM32L0xx/Source/%.c Drivers/CMSIS/Device/ST/STM32L0xx/Source/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m0plus -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32L072xx -DDEBUG -DUSE_B_L072Z_LRWAN1 -DREGION_IN865 -c -I"/home/sherin/Documents/stm/bmp_generic_code/_B-L072z-lrwan1_generic_lorawan-master/App/Core/Inc" -I"/home/sherin/Documents/stm/bmp_generic_code/_B-L072z-lrwan1_generic_lorawan-master/App/bmp180" -I"/home/sherin/Documents/stm/bmp_generic_code/_B-L072z-lrwan1_generic_lorawan-master/App/LoRaWAN/inc" -I"/home/sherin/Documents/stm/bmp_generic_code/_B-L072z-lrwan1_generic_lorawan-master/Drivers/BSP/CMWX1ZZABZ-0xx" -I../Drivers/CMSIS/Device/ST/STM32L0xx/Include -I../Drivers/CMSIS/Include -I../Drivers/STM32L0xx_HAL_Driver/Inc -I"/home/sherin/Documents/stm/bmp_generic_code/_B-L072z-lrwan1_generic_lorawan-master/Drivers/Middlewares/LoRaWAN/Crypto" -I"/home/sherin/Documents/stm/bmp_generic_code/_B-L072z-lrwan1_generic_lorawan-master/Drivers/Middlewares/LoRaWAN/Mac" -I"/home/sherin/Documents/stm/bmp_generic_code/_B-L072z-lrwan1_generic_lorawan-master/Drivers/Middlewares/LoRaWAN/Mac/region" -I"/home/sherin/Documents/stm/bmp_generic_code/_B-L072z-lrwan1_generic_lorawan-master/Drivers/Middlewares/LoRaWAN/Patterns/Basic" -I"/home/sherin/Documents/stm/bmp_generic_code/_B-L072z-lrwan1_generic_lorawan-master/Drivers/Middlewares/LoRaWAN/Phy" -I"/home/sherin/Documents/stm/bmp_generic_code/_B-L072z-lrwan1_generic_lorawan-master/Drivers/Middlewares/LoRaWAN/Utilities" -I"/home/sherin/Documents/stm/bmp_generic_code/_B-L072z-lrwan1_generic_lorawan-master/Drivers/BSP/B-L072Z-LRWAN1" -I"/home/sherin/Documents/stm/bmp_generic_code/_B-L072z-lrwan1_generic_lorawan-master/Drivers/BSP/Components/sx1276" -I"/home/sherin/Documents/stm/bmp_generic_code/_B-L072z-lrwan1_generic_lorawan-master/Drivers/bmp180" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -fcyclomatic-complexity -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

clean: clean-Drivers-2f-CMSIS-2f-Device-2f-ST-2f-STM32L0xx-2f-Source

clean-Drivers-2f-CMSIS-2f-Device-2f-ST-2f-STM32L0xx-2f-Source:
	-$(RM) ./Drivers/CMSIS/Device/ST/STM32L0xx/Source/system_stm32l0xx.cyclo ./Drivers/CMSIS/Device/ST/STM32L0xx/Source/system_stm32l0xx.d ./Drivers/CMSIS/Device/ST/STM32L0xx/Source/system_stm32l0xx.o ./Drivers/CMSIS/Device/ST/STM32L0xx/Source/system_stm32l0xx.su

.PHONY: clean-Drivers-2f-CMSIS-2f-Device-2f-ST-2f-STM32L0xx-2f-Source

